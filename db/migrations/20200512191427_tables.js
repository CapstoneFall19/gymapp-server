
exports.up = knex => {
  return knex.schema
    .createTable('gym', table => {
      table.increments('gym_id').primary();
      table.string('name').notNullable();
      table.string('owner').notNullable();
      table.string('phone').notNullable();
    })
    .createTable('client', table => {
      table.increments('client_id').primary();
      table.integer('gym_id').notNullable();
      table.string('name').notNullable();
      table.string('phone').notNullable();
      table.foreign('gym_id').references('gym.gym_id');
    })
    .createTable('exercise', table => {
      table.increments('exercise_id').primary();
      table.string('name').notNullable();
      table.text('description').notNullable();
    })
    .createTable('max_lift', table => {
      table.integer('client_id').notNullable();
      table.integer('exercise_id').notNullable();
      table.integer('weight').notNullable();
      table.primary(['client_id', 'exercise_id']);
      table.foreign('client_id').references('client.client_id');
      table.foreign('exercise_id').references('exercise.exercise_id');
    })
};

exports.down = knex => {
  return knex.schema
    .dropTable('gym')
    .dropTable('client')
    .dropTable('exercise')
    .dropTable('max_lift')
};
