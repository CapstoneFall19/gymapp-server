const gym = require('./data/gym');
const client = require('./data/client');
const exercise = require('./data/exercise');
const maxLift = require ('./data/maxLift');

async function deleteTables(knex) {
  await knex('max_lift').del();
  await knex('exercise').del();
  await knex('client').del();
  await knex('gym').del();
}

async function seedTables(knex){
  await knex('gym').insert(gym);
  await knex('client').insert(client);
  await knex('exercise').insert(exercise);
  await knex('max_lift').insert(maxLift);
}

exports.seed = async knex => {
  await deleteTables(knex);
  await seedTables(knex);
}