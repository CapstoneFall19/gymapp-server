module.exports = [
  {
    name: "Bench press",
    description: "Lie on a flat bench and grasp the bar slightly wider than shoulder " +
      "width. Lower the bar to your chest while keeping your glutes and abs tightened, " +
      "your elbows slightly tucked, and your back arched. When the bar touches your body," +
      " drive your feet into the floor to press the bar back up. Adjust your weights " +
      "accordingly for each set."
  },
  {
    name: "Overhead press",
    description: "Set the bar up in a squat rack or cage, and grasp it just outside " +
      "shoulder width. Take the bar off the rack and hold it at shoulder level with " +
      "your forearms vertical. Squeeze the bar and brace your abs. Press the bar overhead, " +
      "pushing your head forward and shrugging your traps as the bar passes your face."
  },
  {
    name: "Incline bench press",
    description: "Set an adjustable bench to a 30- to 45-degree angle and lie back on" +
      " it with a dumbbell in each hand at shoulder level. Then arch your back, drive " +
      "your feet into the floor, and press the weights over your chest."
  },
  {
    name: "Clean and press",
    description: "Stand with feet shoulder width apart. Then, with your lower back arched, " +
      "bend your hips back to lower your torso and grasp the bar with hands shoulder width. " +
      "Extend your hips to lift the bar off the floor. When it gets past your knees, jump and " +
      "shrug the bar so that momentum raises it and you catch it at shoulder level. " +
      "Brace your abs and stand tall. Press the bar straight overhead."
  },
  {
    name: "Landmine press",
    description: "Wedge the end of a barbell into a corner, or load it into a landmine station. " +
      "Load the opposite end with weight and grasp it toward the end of the sleeve with your left hand. " +
      "Stand with feet shoulder width and press the bar."
  },
  {
    name: "One arm row",
    description: "Grab a dumbbell in one hand and stand in a staggered stance with one foot forward. " +
      "Bend at your hips and knees and lower your torso until it’s almost parallel to the floor. " +
      "Let the dumbbell hang at arm’s length from your shoulder. Without moving your torso, pull the dumbbell " +
      "to the side of your torso, keeping elbow close to your side. Pause and squeeze " +
      "at top of movement. Lower dumbbell back to start position"
  }
];