module.exports = [
  {
    name: "Test Gym",
    owner: "Josh",
    phone: "1234567",
  },
  {
    name: "Gold's Gym",
    owner: "Rudy Gold",
    phone: "5036667777",
  },
  {
    name: "PSU Rec Center",
    owner: "PSU",
    phone: "5035551234",
  },
];