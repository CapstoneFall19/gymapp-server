module.exports = [
  {
    gym_id: 1,
    name: "Carter Wilson",
    phone: "5037654321",
  },
  {
    gym_id: 1,
    name: "Steve Mansfield",
    phone: "5031234567",
  },
  {
    gym_id: 1,
    name: "Elizabeth Williams",
    phone: "5038675309",
  },
  {
    gym_id: 2,
    name: "Billy Maddison",
    phone: "5419865467",
  },
  {
    gym_id: 2,
    name: "Happy Gilmore",
    phone: "7075689635",
  },
  {
    gym_id: 3,
    name: "Scotty Wallace",
    phone: "9458675556",
  },
  {
    gym_id: 3,
    name: "Squirley Dan",
    phone: "7894561230",
  }
];