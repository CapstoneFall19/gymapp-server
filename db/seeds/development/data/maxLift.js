module.exports = [
  {
    client_id: 1,
    exercise_id: 1,
    weight: 180,
  },
  {
    client_id: 1,
    exercise_id: 2,
    weight: 220,
  },
  {
    client_id: 1,
    exercise_id: 3,
    weight: 100,
  },
  {
    client_id: 2,
    exercise_id: 1,
    weight: 225,
  },
  {
    client_id: 2,
    exercise_id: 4,
    weight: 185,
  },
  {
    client_id: 3,
    exercise_id: 1,
    weight: 175,
  },
  {
    client_id: 4,
    exercise_id: 1,
    weight: 133,
  },
  {
    client_id: 4,
    exercise_id: 6,
    weight: 140,
  },
  {
    client_id: 5,
    exercise_id: 1,
    weight: 250,
  },
  {
    client_id: 5,
    exercise_id: 2,
    weight: 225,
  },
  {
    client_id: 5,
    exercise_id: 3,
    weight: 230,
  },
  {
    client_id: 5,
    exercise_id: 4,
    weight: 190,
  },
  {
    client_id: 5,
    exercise_id: 5,
    weight: 175,
  },
  {
    client_id: 6,
    exercise_id: 6,
    weight: 125,
  },
  {
    client_id: 6,
    exercise_id: 1,
    weight: 110,
  },
  {
    client_id: 7,
    exercise_id: 1,
    weight: 180,
  },
  {
    client_id: 7,
    exercise_id: 5,
    weight: 200,
  },
]