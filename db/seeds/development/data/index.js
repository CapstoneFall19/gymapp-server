const gym = require('./gym');
const client = require('./client');
const exercise = require('./exercise');
const maxLift = require ('./maxLift');

module.exports = {
  gym,
  client,
  exercise,
  maxLift,
};