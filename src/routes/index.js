const express = require('express');
const router = express.Router();

router.use('/gym', require('./gym'));
router.use('/client', require('./client'));
router.use('/exercise', require('./exercise'));
router.use('/maxLift', require('./maxLift'));

module.exports = router;