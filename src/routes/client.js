const express = require('express');
const router = express.Router();
const { SERVER_URL } = require('../config');
const {
  addClient,
  getClients,
  getClientsByGym,
  getClient,
  updateClient,
  deleteClient,
  messageResponses,
  FOREIGN_KEY_VIOLATION,
  UNIQUENESS_VIOLATION,
} = require('../database');

router.get('/', async(req,res)=>{
  return await getClients()
    .then(data => {
      if (data.length === 0) {
        console.info('Client table is empty');
        return res.status(404).send({ message: messageResponses[404] });
      }

      console.info('Successfully retrieved client list from the database');
      return res.status(200).send(data);
    })
    .catch(err => {
      console.error(`Error retrieving clients: ${err}`);
      return res
        .status(500)
        .send({ message: messageResponses[500], error: err.message });
    });
});

router.get('/:id', async(req,res) => {
  if (!Number.isInteger(parseInt(req.params.id))) {
    return res.status(400).send({ message: messageResponses[400] });
  }
  return await getClient(req.params.id)
    .then(data => {
      console.info('Successfully retrieved client from database');
      return res.status(200).send(data);
    })
    .catch(err => {
      if (err.result && err.result.rowCount === 0) {
        console.info(`Found no client in the database with id ${req.params.id}`);
        return res.status(404).send({ message: messageResponses[404] });
      }

      console.error(`Error retrieving client: ${err}`);
      return res
        .status(500)
        .send({ message: messageResponses[500], error: err.message });
    });
})

router.get('/gym/:gym', async(req,res) => {
  if (!Number.isInteger(parseInt(req.params.gym))) {
    return res.status(400).send({ message: messageResponses[400] });
  }
  return await getClientsByGym(req.params.gym)
    .then(data => {
      console.info('Successfully retrieved client from database');
      return res.status(200).send(data);
    })
    .catch(err => {
      if (err.result && err.result.rowCount === 0) {
        console.info(`Found no client in the database with id ${req.params.gym}`);
        return res.status(404).send({ message: messageResponses[404] });
      }

      console.error(`Error retrieving client: ${err}`);
      return res
        .status(500)
        .send({ message: messageResponses[500], error: err.message });
    });
})

router.post('/', async (req,res) => {
  if (
    !req.body ||
    !req.body.name ||
    !req.body.gymId ||
    !req.body.phone
  ) {
    return res.status(400).send({ message: messageResponses[400] });
  }

  const {
    name,
    gymId,
    phone,
  } = req.body;

  return await addClient(gymId, name, phone)
    .then(result => {
      console.info('Added client to database');

      let e = {};

      if (Array.isArray(result) && result.length) {
        e = { return: result[0].client_id };
      } else {
        e = { return: result.client_id };
      }

      return res
        .set('Location', `${SERVER_URL}/api/client/${e.return}`)
        .status(201)
        .send({ message: messageResponses[201] });
    })
    .catch(err => {
      let code;
      let message;
      let detail;

      if (!err.stat) {
        code = err.code;
        message = err.message;
        detail = err.detail;
      } else {
        code = err.first.code;
        message = err.first.message;
        detail = err.first.detail;
      }

      if ([FOREIGN_KEY_VIOLATION, UNIQUENESS_VIOLATION].includes(code)) {
        const hint = 'Attempted to add client with invalid keys';
        console.error(
          `Attempted to add client with invalid keys:\n ${message} \n ${detail}`
        );
        return res
          .status(409)
          .send({ message: message, error: detail, hint: hint });
      }

      console.error(`Error adding client member to database:\n ${err}`);
      return res.status(500).send({
        message: messageResponses[500],
        error: err.message,
      });
    });
});

router.put('/', async (req, res) => {
  if (
    !req.body ||
    !req.body.id ||
    !req.body.gymId ||
    !req.body.name ||
    !req.body.phone
  ) {
    return res.status(400).send({ message: messageResponses[400] });
  }

  const {
    id,
    name,
    gymId,
    phone,
  } = req.body;

  return await updateClient(
    id,
    gymId,
    name,
    phone,
  )
    .then(result => {
      if (!result.rowCount) {
        console.info(
          `Unable to update client record, id ${id} does not exist`
        );
        return res.status(404).send({ message: messageResponses[404] });
      }
      console.info('Updated client to database');

      console.info(`Updated client with id ${id}`);
      return res
        .set('Location', `${SERVER_URL}/api/client/${id}`)
        .status(200)
        .send({ message: messageResponses[200] });
    })
    .catch(err => {
      let code;
      let message;
      let detail;

      if (!err.stat) {
        code = err.code;
        message = err.message;
        detail = err.detail;
      } else {
        code = err.first.code;
        message = err.first.message;
        detail = err.first.detail;
      }

      if ([FOREIGN_KEY_VIOLATION, UNIQUENESS_VIOLATION].includes(code)) {
        const hint = 'Attempted to update client with invalid keys';
        console.error(
          `Attempted to update client with invalid keys:\n ${message} \n ${detail}`
        );
        return res
          .status(409)
          .send({ message: message, error: detail, hint: hint });
      }

      console.error(`Error updating client to database:\n ${err}`);
      return res.status(500).send({
        message: messageResponses[500],
        error: err.message,
      });
    });
})

router.delete('/:id', async (req,res) => {
  return await deleteClient(req.params.id)
    .then(result => {
      if(result[0].rowCount === 0){
        console.info(
          `No client found for id ${req.params.id}`
        );
        return res.status(404).send({ message: messageResponses[404] });
      }
      console.info('Successfully removed client from database');
      return res.status(200).send({ message: messageResponses[200] });
    })
    .catch(err => {
      console.error(`Error deleting client: ${err}`);
      return res
        .status(500)
        .send({ message: messageResponses[500], error: err.message });
    });
})

module.exports = router;