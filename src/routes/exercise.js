const express = require('express');
const router = express.Router();
const { SERVER_URL } = require('../config');
const {
  addExercise,
  getExercises,
  getExercise,
  updateExercise,
  messageResponses,
  FOREIGN_KEY_VIOLATION,
  UNIQUENESS_VIOLATION,
} = require('../database');

router.get('/', async(req,res)=>{
  return await getExercises()
    .then(data => {
      if (data.length === 0) {
        console.info('Exercise table is empty');
        return res.status(404).send({ message: messageResponses[404] });
      }

      console.info('Successfully retrieved exercise list from the database');
      return res.status(200).send(data);
    })
    .catch(err => {
      console.error(`Error retrieving exercises: ${err}`);
      return res
        .status(500)
        .send({ message: messageResponses[500], error: err.message });
    });
});

router.get('/:id', async(req,res) => {
  if (!Number.isInteger(parseInt(req.params.id))) {
    return res.status(400).send({ message: messageResponses[400] });
  }
  return await getExercise(req.params.id)
    .then(data => {
      console.info('Successfully retrieved exercise from database');
      return res.status(200).send(data);
    })
    .catch(err => {
      if (err.result && err.result.rowCount === 0) {
        console.info(`Found no exercise in the database with id ${req.params.id}`);
        return res.status(404).send({ message: messageResponses[404] });
      }

      console.error(`Error retrieving exercise: ${err}`);
      return res
        .status(500)
        .send({ message: messageResponses[500], error: err.message });
    });
})

router.post('/', async (req,res) => {
  if (
    !req.body ||
    !req.body.name ||
    !req.body.description
  ) {
    return res.status(400).send({ message: messageResponses[400] });
  }

  const {
    name,
    description,
  } = req.body;

  return await addExercise(name, description)
    .then(result => {
      console.info('Added exercise to database');

      let e = {};

      if (Array.isArray(result) && result.length) {
        e = { return: result[0].exercise_id };
      } else {
        e = { return: result.exercise_id };
      }

      return res
        .set('Location', `${SERVER_URL}/api/exercise/${e.return}`)
        .status(201)
        .send({ message: messageResponses[201] });
    })
    .catch(err => {
      let code;
      let message;
      let detail;

      if (!err.stat) {
        code = err.code;
        message = err.message;
        detail = err.detail;
      } else {
        code = err.first.code;
        message = err.first.message;
        detail = err.first.detail;
      }

      if ([FOREIGN_KEY_VIOLATION, UNIQUENESS_VIOLATION].includes(code)) {
        const hint = 'Attempted to add exercise with invalid keys';
        console.error(
          `Attempted to add exercise with invalid keys:\n ${message} \n ${detail}`
        );
        return res
          .status(409)
          .send({ message: message, error: detail, hint: hint });
      }

      console.error(`Error adding exercise to database:\n ${err}`);
      return res.status(500).send({
        message: messageResponses[500],
        error: err.message,
      });
    });
});

router.put('/', async (req, res) => {
  if (
    !req.body ||
    !req.body.id ||
    !req.body.name ||
    !req.body.description
  ) {
    return res.status(400).send({ message: messageResponses[400] });
  }

  const {
    id,
    name,
    description,
  } = req.body;

  return await updateExercise(
    id,
    name,
    description,
  )
    .then(result => {
      if (!result.rowCount) {
        console.info(
          `Unable to update exercise record, id ${id} does not exist`
        );
        return res.status(404).send({ message: messageResponses[404] });
      }
      console.info('Updated exercise to database');

      console.info(`Updated exercise with id ${id}`);
      return res
        .set('Location', `${SERVER_URL}/api/exercise/${id}`)
        .status(200)
        .send({ message: messageResponses[200] });
    })
    .catch(err => {
      let code;
      let message;
      let detail;

      if (!err.stat) {
        code = err.code;
        message = err.message;
        detail = err.detail;
      } else {
        code = err.first.code;
        message = err.first.message;
        detail = err.first.detail;
      }

      if ([FOREIGN_KEY_VIOLATION, UNIQUENESS_VIOLATION].includes(code)) {
        const hint = 'Attempted to update exercise with invalid keys';
        console.error(
          `Attempted to update exercise with invalid keys:\n ${message} \n ${detail}`
        );
        return res
          .status(409)
          .send({ message: message, error: detail, hint: hint });
      }

      console.error(`Error adding exercise to database:\n ${err}`);
      return res.status(500).send({
        message: messageResponses[500],
        error: err.message,
      });
    });
})

module.exports = router;