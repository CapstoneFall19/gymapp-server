const express = require('express');
const router = express.Router();
const { SERVER_URL } = require('../config');
const {
  addLift,
  getAllLifts,
  getLiftsByClient,
  getLiftByClient,
  updateLift,
  messageResponses,
  FOREIGN_KEY_VIOLATION,
  UNIQUENESS_VIOLATION,
} = require('../database');

router.get('/', async(req,res)=>{
  return await getAllLifts()
    .then(data => {
      if (data.length === 0) {
        console.info('max lift table is empty');
        return res.status(404).send({ message: messageResponses[404] });
      }

      console.info('Successfully retrieved max lift list from the database');
      return res.status(200).send(data);
    })
    .catch(err => {
      console.error(`Error retrieving max lifts: ${err}`);
      return res
        .status(500)
        .send({ message: messageResponses[500], error: err.message });
    });
});

router.get('/:id', async(req,res) => {
  if (!Number.isInteger(parseInt(req.params.id))) {
    return res.status(400).send({ message: messageResponses[400] });
  }
  return await getLiftsByClient(req.params.id)
    .then(data => {
      console.info('Successfully retrieved max lifts from database');
      return res.status(200).send(data[0].json_agg);
    })
    .catch(err => {
      if (err.result && err.result.rowCount === 0) {
        console.info(`Found no client lifts in the database with id ${req.params.id}`);
        return res.status(404).send({ message: messageResponses[404] });
      }

      console.error(`Error retrieving client lifts: ${err}`);
      return res
        .status(500)
        .send({ message: messageResponses[500], error: err.message });
    });
})

router.get('/:id/:lift', async(req,res) => {
  if (!Number.isInteger(parseInt(req.params.id)) || !Number.isInteger(parseInt(req.params.lift))) {
    return res.status(400).send({ message: messageResponses[400] });
  }
  return await getLiftByClient(req.params.id, req.params.lift)
    .then(data => {
      console.info('Successfully retrieved client lift from database');
      return res.status(200).send(data);
    })
    .catch(err => {
      if (err.result && err.result.rowCount === 0) {
        console.info(`Found no client lift in the database with id ${req.params.lift}`);
        return res.status(404).send({ message: messageResponses[404] });
      }

      console.error(`Error retrieving client lift: ${err}`);
      return res
        .status(500)
        .send({ message: messageResponses[500], error: err.message });
    });
})

router.post('/', async (req,res) => {
  if (
    !req.body ||
    !req.body.clientId ||
    !req.body.exerciseId ||
    !req.body.weight
  ) {
    return res.status(400).send({ message: messageResponses[400] });
  }

  const {
    clientId,
    exerciseId,
    weight
  } = req.body;

  return await addLift(clientId, exerciseId, weight)
    .then(result => {
      console.info('Added max lift to database');

      let e = {};

      if (Array.isArray(result) && result.length) {
        e = { return: result[0].client_id };
      } else {
        e = { return: result.client_id };
      }

      return res
        .set('Location', `${SERVER_URL}/api/maxLift/${e.return}`)
        .status(201)
        .send({ message: messageResponses[201] });
    })
    .catch(err => {
      let code;
      let message;
      let detail;

      if (!err.stat) {
        code = err.code;
        message = err.message;
        detail = err.detail;
      } else {
        code = err.first.code;
        message = err.first.message;
        detail = err.first.detail;
      }

      if ([FOREIGN_KEY_VIOLATION, UNIQUENESS_VIOLATION].includes(code)) {
        const hint = 'Attempted to add max lift with invalid keys';
        console.error(
          `Attempted to add max lift with invalid keys:\n ${message} \n ${detail}`
        );
        return res
          .status(409)
          .send({ message: message, error: detail, hint: hint });
      }

      console.error(`Error adding max lift to database:\n ${err}`);
      return res.status(500).send({
        message: messageResponses[500],
        error: err.message,
      });
    });
});

router.put('/', async (req, res) => {
  if (
    !req.body ||
    !req.body.clientId ||
    !req.body.exerciseId ||
    !req.body.weight
  ) {
    return res.status(400).send({ message: messageResponses[400] });
  }

  const {
    clientId,
    exerciseId,
    weight
  } = req.body;

  return await updateLift(
    clientId,
    exerciseId,
    weight
  )
    .then(result => {
      if (!result.rowCount) {
        console.info(
          `Unable to update max lift record, exercise id ${exerciseId} or client id ${clientId} does not exist `
        );
        return res.status(404).send({ message: messageResponses[404] });
      }
      console.info('Updated max lift to database');

      console.info(`Updated max lift with id ${exerciseId}`);
      return res
        .set('Location', `${SERVER_URL}/api/maxLift/${clientId}`)
        .status(200)
        .send({ message: messageResponses[200] });
    })
    .catch(err => {
      let code;
      let message;
      let detail;

      if (!err.stat) {
        code = err.code;
        message = err.message;
        detail = err.detail;
      } else {
        code = err.first.code;
        message = err.first.message;
        detail = err.first.detail;
      }

      if ([FOREIGN_KEY_VIOLATION, UNIQUENESS_VIOLATION].includes(code)) {
        const hint = 'Attempted to update max lift with invalid keys';
        console.error(
          `Attempted to update max lift with invalid keys:\n ${message} \n ${detail}`
        );
        return res
          .status(409)
          .send({ message: message, error: detail, hint: hint });
      }

      console.error(`Error updating max lift to database:\n ${err}`);
      return res.status(500).send({
        message: messageResponses[500],
        error: err.message,
      });
    });
})

module.exports = router;