const express = require('express');
const router = express.Router();
const { SERVER_URL } = require('../config');
const {
  getGym,
  getGyms,
  addGym,
  updateGym,
  messageResponses,
  FOREIGN_KEY_VIOLATION,
  UNIQUENESS_VIOLATION,
} = require('../database');

router.get('/', async(req,res)=>{
  return await getGyms()
    .then(data => {
      if (data.length === 0) {
        console.info('Gym table is empty');
        return res.status(404).send({ message: messageResponses[404] });
      }

      console.info('Successfully retrieved gym list from the database');
      return res.status(200).send(data);
    })
    .catch(err => {
      console.error(`Error retrieving gyms: ${err}`);
      return res
        .status(500)
        .send({ message: messageResponses[500], error: err.message });
    });
})

router.get('/:id', async(req,res) => {
  if (!Number.isInteger(parseInt(req.params.id))) {
    return res.status(400).send({ message: messageResponses[400] });
  }
  return await getGym(req.params.id)
    .then(data => {
      console.info('Successfully retrieved gym from database');
      return res.status(200).send(data);
    })
    .catch(err => {
      if (err.result && err.result.rowCount === 0) {
        console.info(`Found no gym in the database with id ${req.params.id}`);
        return res.status(404).send({ message: messageResponses[404] });
      }

      console.error(`Error retrieving gym: ${err}`);
      return res
        .status(500)
        .send({ message: messageResponses[500], error: err.message });
    });
})

router.post('/', async (req,res) => {
  if (
    !req.body ||
    !req.body.name ||
    !req.body.owner ||
    !req.body.phone
  ) {
    return res.status(400).send({ message: messageResponses[400] });
  }

  const {
    name,
    owner,
    phone,
  } = req.body;

  return await addGym(name, owner, phone)
    .then(result => {
      console.info('Added gym to database');

      let e = {};

      if (Array.isArray(result) && result.length) {
        e = { return: result[0].gym_id };
      } else {
        e = { return: result.gym_id };
      }

      return res
        .set('Location', `${SERVER_URL}/api/gym/${e.return}`)
        .status(201)
        .send({ message: messageResponses[201] });
    })
    .catch(err => {
      let code;
      let message;
      let detail;

      if (!err.stat) {
        code = err.code;
        message = err.message;
        detail = err.detail;
      } else {
        code = err.first.code;
        message = err.first.message;
        detail = err.first.detail;
      }

      if ([FOREIGN_KEY_VIOLATION, UNIQUENESS_VIOLATION].includes(code)) {
        const hint = 'Attempted to add gym with invalid keys';
        console.error(
          `Attempted to add gym with invalid keys:\n ${message} \n ${detail}`
        );
        return res
          .status(409)
          .send({ message: message, error: detail, hint: hint });
      }

      console.error(`Error adding gym member to database:\n ${err}`);
      return res.status(500).send({
        message: messageResponses[500],
        error: err.message,
      });
    });
});

router.put('/', async (req, res) => {
  if (
    !req.body ||
    !req.body.id ||
    !req.body.name ||
    !req.body.owner ||
    !req.body.phone
  ) {
    return res.status(400).send({ message: messageResponses[400] });
  }

  const {
    id,
    name,
    owner,
    phone,
  } = req.body;

  return await updateGym(
    id,
    name,
    owner,
    phone,
  )
    .then(result => {
      if (!result.rowCount) {
        console.info(
          `Unable to update gym record, id ${id} does not exist`
        );
        return res.status(404).send({ message: messageResponses[404] });
      }
      console.info('Updated gym to database');

      console.info(`Updated gym with id ${id}`);
      return res
        .set('Location', `${SERVER_URL}/api/gym/${id}`)
        .status(200)
        .send({ message: messageResponses[200] });
    })
    .catch(err => {
      let code;
      let message;
      let detail;

      if (!err.stat) {
        code = err.code;
        message = err.message;
        detail = err.detail;
      } else {
        code = err.first.code;
        message = err.first.message;
        detail = err.first.detail;
      }

      if ([FOREIGN_KEY_VIOLATION, UNIQUENESS_VIOLATION].includes(code)) {
        const hint = 'Attempted to update gym with invalid keys';
        console.error(
          `Attempted to update gym with invalid keys:\n ${message} \n ${detail}`
        );
        return res
          .status(409)
          .send({ message: message, error: detail, hint: hint });
      }

      console.error(`Error adding gym to database:\n ${err}`);
      return res.status(500).send({
        message: messageResponses[500],
        error: err.message,
      });
    });
})

module.exports = router;