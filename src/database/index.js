const queries = require('./queries');

module.exports = {
  ...queries,
  FOREIGN_KEY_VIOLATION: '23503',
  UNIQUENESS_VIOLATION: '23505',
  CHECK_VIOLATION: '23514',
  messageResponses: {
    400: 'Bad Request',
    200: 'Success',
    201: 'Success',
    500: 'Internal Server Error',
    404: 'Resource Not Found',
    409: 'Conflict',
  },
};