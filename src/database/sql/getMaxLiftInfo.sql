/*
    Gets max lift weight with exercise name and description
*/

SELECT json_agg(
    json_build_object(
    'name', e.name,
    'description', e.description,
    'weight', ml.weight)
) FROM max_lift ml
INNER JOIN
    exercise e on ml.exercise_id = e.exercise_id
WHERE ml.client_id = $1
