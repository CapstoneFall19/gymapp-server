const {
  loadDatabaseConnection,
  maxLift,
} = require('./connection');

//GYM TABLE QUERIES
async function getGym(id){
  const connection = loadDatabaseConnection();

  return connection.one(
    'SELECT * from gym WHERE gym_id=$1',[id]
  );
}

async function getGyms() {
  const connection = loadDatabaseConnection();

  return connection.any(
    'SELECT * from gym ORDER BY name ASC'
  );
}

async function addGym(name, owner, phone){
  const connection = loadDatabaseConnection();
  return connection.one(
    'INSERT INTO gym(name,owner,phone) VALUES($1,$2,$3) RETURNING gym_id',
    [name,owner,phone]
  )
}

async function updateGym(id, name, owner, phone){
  const connection = loadDatabaseConnection();
  return connection.tx(()=>{
    return connection.result(
      'UPDATE gym SET name=$1, owner=$2, phone=$3 WHERE gym_id=$4',
      [name, owner, phone, id]
    );
  });
}


//CLIENT TABLE QUERIES
async function addClient(gymId, name, phone){
  const connection = loadDatabaseConnection();
  return connection.one(
    'INSERT INTO client(gym_id,name,phone) VALUES($1, $2, $3) RETURNING client_id',
    [gymId, name, phone]
  )
}

async function getClient(id){
  const connection = loadDatabaseConnection();
  return connection.one(
    'SELECT * from client WHERE client_id=$1',
    [id]
  );
}

async function getClients(){
  const connection = loadDatabaseConnection();
  return connection.any(
    'SELECT * from client ORDER BY name ASC'
  );
}

async function getClientsByGym(gymId){
  const connection = loadDatabaseConnection();
  return connection.any(
    'SELECT * from client WHERE gym_id=$1',
    [gymId]
  );
}

async function updateClient(id, gymId, name, phone,){
  const connection = loadDatabaseConnection();
  return connection.tx(()=>{
    return connection.result(
      'UPDATE client SET gym_id=$2, name=$3, phone=$4 WHERE client_id=$1',
      [id, gymId, name, phone]
    )
  })
}


//EXERCISE QUERIES
async function getExercises() {
  const connection = loadDatabaseConnection();
  return connection.any(
    'SELECT * from exercise ORDER BY name ASC'
  );
}

async function getExercise(id){
  const connection = loadDatabaseConnection();
  return connection.one(
    'SELECT * from exercise WHERE exercise_id=$1',
    [id]
  );
}

async function updateExercise(id, name, description){
  const connection = loadDatabaseConnection();
  return connection.tx(()=>{
    return connection.result(
      'UPDATE exercise SET name=$2, description=$3 WHERE exercise_id=$1',
      [id, name, description]
    );
  });
}

async function addExercise(name, description){
  const connection = loadDatabaseConnection();
  return connection.one(
    'INSERT INTO exercise(name, description) VALUES($1,$2) RETURNING exercise_id',
    [name, description]
  )
}

async function getLiftsByClient(id){
  const connection = loadDatabaseConnection();
  const query = maxLift.info;
  return connection.any(
    query,
    [id], maxLiftInfo =>{
      if(maxLiftInfo){
        return maxLiftInfo.json_agg;
      } else {
        return null;
      }
    }
  );
}

async function getLiftByClient(clientId, exerciseId){
  const connection = loadDatabaseConnection();
  return connection.one(
    'SELECT * from max_lift WHERE client_id=$1 AND exercise_id=$2',
    [clientId, exerciseId]
  );
}

async function getAllLifts(){
  const connection = loadDatabaseConnection();
  return connection.any(
    'SELECT * from max_lift ORDER BY client_id ASC'
  );
}

async function addLift(clientId, exerciseId, weight){
  const connection = loadDatabaseConnection();
  return connection.one(
    'INSERT into max_lift(client_id, exercise_id, weight) VALUES($1, $2, $3) RETURNING client_id',
    [clientId, exerciseId, weight]
  );
}

async function updateLift(clientId, exerciseId, weight){
  const connection = loadDatabaseConnection();
  return connection.tx(()=>{
    return connection.result(
      'UPDATE max_lift SET weight=$3 WHERE client_id=$1 AND exercise_id=$2',
      [clientId, exerciseId, weight]
    );
  });
}

async function deleteClient(clientId){
  const connection = loadDatabaseConnection();
  return connection.tx(t =>{
    return t.batch([
      t.result(
        'DELETE FROM max_lift WHERE client_id = $1', [clientId]
      ),
      t.result(
        'DELETE FROM client WHERE client_id = $1', [clientId]
      )
      ]
    )
  })
}

module.exports = {
  getGym,
  getGyms,
  addGym,
  updateGym,
  addClient,
  getClient,
  getClients,
  getClientsByGym,
  updateClient,
  getExercise,
  getExercises,
  updateExercise,
  addExercise,
  getLiftByClient,
  getLiftsByClient,
  getAllLifts,
  addLift,
  updateLift,
  deleteClient
}